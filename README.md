# Array.prototype.overflow

Adds a function called **overflow** to Array and all built-in numeric arrays as well.

That means if you import it and hook-it to Array, then you can get the last element of an array with -1, just like in Python.

```javascript
let last_element = ["first", "last"].overflow(-1);
// last_element = "last"

...
```

But unlike python you can overflow the boundaries more than once.

```javascript
let last_element = ["first", "last"].overflow(-3);
// last_element = "last"

...
```

Overflow literally ties both ends of the array together and makes it a circular buffer.

```javascript
import { hook } from 'array.prototype.overflow'
hook()

let array = ['first', 'last']
let string = ''
for (let i = 0; i < 10; i++) {
    string += array.overflow(i) + ', '
}
console.log(string.slice(0, -2))
//    first, last, first, last, first, last, first, last, first, last
```

| **Note**                                                                             |
| :----------------------------------------------------------------------------------- |
| _When testing the code I ran into problems with indexes above 9.000.000.000.000.000_ |

## Install

It's easy.

```bash
npm install --save array.prototype.overflow

```

## Use

Import and hook it ...

```javascript
import { hook } from 'array.prototype.overflow';
hook();

...
```

Or require it, the old fashion way ...

```javascript
var overflow = require('array.prototype.overflow');
overflow.hook();

...
```

## The Array.prototype.of() shorthand

There is a shorthand function called .of() that you can use instead of .overflow(), but you will have to import of instead of hook, like this:

```javascript
import { of } from 'array.prototype.overflow';
of();

let last_element = ["first", "last"].of(-1);
// last_element = "last"

...
```

Or the old fashion way ...

```javascript
var overflow = require('array.prototype.overflow');
overflow.of();

let last_element = ["first", "last"].of(-1);
// last_element = "last"

...
```

| **Note**                                                                                                                                                                                                                                                                                                                                                  |
| :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| _JavaScript's Array already has a function called .of(), but it's not a part of the prototype, so it's safe to use. If however you decide to use it, know that people could get confused as the functions are not related in any way._ [More on Array.of here](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/of) |

## Documentation

[Documentation](https://gitlab.com/thro/array.prototype.overflow/-/blob/master/index.md)

## License

[MIT](https://mit-license.org/)
