/**
 *
 * Just by importing the module you will affect the global scope of the script's runtime environment.
 *
 * So first we setup the environment than we test the functions.
 *
 */

// Just by importing the module, Array.overflow() is added to the Array prototype
import { hook, of, shorthand } from './index'

// Hooking into Array for all tests
hook(false)

/**
 *
 * Now we test the functions set above.
 *
 */

test('Last element of an array should be returned by using .overflow()', () => {
    let array = ['first', 'middle', 'last']

    expect(array.overflow(5)).toBe('last')
    expect(array.overflow(2)).toBe('last')
    expect(array.overflow(-1)).toBe('last')
    expect(array.overflow(-4)).toBe('last')
})

test('Built-in numeric arrays should not be hooked when hook(false) is called', () => {
    expect(new Int8Array([1, 2, 3]).overflow).toBeUndefined()
})

test('Shorthand .of() should exist in Array', () => {
    of()

    let array = ['first', 'last']

    expect(array.of(-1)).toBe('last')
})

test('Shorthand .aboveflux() should exist in the array', () => {
    // Creating the long shorthand Array.aboveflux() for Array.overflow()
    shorthand('aboveflux')

    expect(['first', 'last'].aboveflux(-1)).toBe('last')
    expect(new Int8Array([1, 2, 3]).of(-1)).toBe(3)
})

test('Not throw an ReferenceError when re-hooking', () => {
    expect(of).not.toThrow(ReferenceError)
})

test('Throw an ReferenceError if of already exists', () => {
    Array.prototype.of = function () {
        'some other function'
    }

    expect(of).toThrow(ReferenceError)
    expect(of).toThrow('Array.prototype.of already exists!')
})

test('Throw an ReferenceError if an function already exists in built-in numeric array', () => {
    Int8Array.prototype.boom = function () {
        'some other function'
    }

    expect(() => shorthand('boom')).toThrow(
        'Int8Array.prototype.boom already exists!'
    )
})

test('Throw an ReferenceError if trying to override a built-in name', () => {
    expect(() => shorthand('length')).toThrow(
        'Array.prototype.length already exists!'
    )
})

test('Last element of an built-in numeric arrays should be returned if -1 by using .overflow()', () => {
    hook()

    expect(new Int8Array([1, 2, 3]).overflow(-1)).toBe(3)
    expect(new Uint8Array([1, 2, 3]).overflow(-1)).toBe(3)
    expect(new Uint8ClampedArray([1, 2, 3]).overflow(-1)).toBe(3)
    expect(new Int16Array([1, 2, 3]).overflow(-1)).toBe(3)
    expect(new Uint16Array([1, 2, 3]).overflow(-1)).toBe(3)
    expect(new Int32Array([1, 2, 3]).overflow(-1)).toBe(3)
    expect(new Uint32Array([1, 2, 3]).overflow(-1)).toBe(3)
    expect(new Float32Array([1, 2, 3]).overflow(-1)).toBe(3)
    expect(new Float64Array([1, 2, 3]).overflow(-1)).toBe(3)
    expect(new BigInt64Array([1n, 2n, 3n]).overflow(-1)).toBe(3n)
    expect(new BigUint64Array([1n, 2n, 3n]).overflow(-1)).toBe(3n)
})
