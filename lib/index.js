'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.hook = hook
exports.of = of
exports.shorthand = shorthand
var _rangedOverflow = require('ranged-overflow')

/**
 * @private
   The core function of this library, returns the value at the corresponding
   index or overflowed index
 * @param {number} [index] The index (overflowed or not) of the value wanted
 * @returns Value wanted from the array
 */
function prototype(index) {
    return this[(0, _rangedOverflow.overflow)(0, this.length - 1, index)]
}

/**
 * @private
 * Returns a list of all built-in numeric arrays types and names.
   NOTE: that this is a function on purpose so the script doesn't throw an
   exception while being imported if one of the built-ins doesn't exist
 * @returns {array} An array of objects with types and the names of built-in
                    numeric arrays in JavaScript,
                    example: [{ type: Int8Array, name: 'Int8Array' }, ...]
 */
function arrays() {
    return [
        { type: Int8Array, name: 'Int8Array' },
        { type: Uint8Array, name: 'Uint8Array' },
        { type: Uint8ClampedArray, name: 'Uint8ClampedArray' },
        { type: Int16Array, name: 'Int16Array' },
        { type: Uint16Array, name: 'Uint16Array' },
        { type: Int32Array, name: 'Int32Array' },
        { type: Uint32Array, name: 'Uint32Array' },
        { type: Float32Array, name: 'Float32Array' },
        { type: Float64Array, name: 'Float64Array' },
        { type: BigInt64Array, name: 'BigInt64Array' },
        { type: BigUint64Array, name: 'BigUint64Array' },
    ]
}
/**
 * Hooks overflow function into Array.prototype (and built-in numeric arrays)
   NOTE: If 'overflow' already exists then this function will throw a
 * ReferenceError exception
 * @param {boolean} [builtInArrays = true] If set to false the function will
                    not hook overflow into the built-in numeric arrays.
                    The built-in arrays are [ Int8Array, Uint8Array,
                    Uint8ClampedArray, Int16Array, Uint16Array, Int32Array,
                    Uint32Array, Float32Array, Float64Array, BigInt64Array,
                    BigUint64Array ]

 */
function hook(builtInArrays = true) {
    shorthand('overflow', builtInArrays)
}

/**
 * Creates the function 'of' in Array.prototype and built-in numeric arrays.
   NOTE: If 'of' already exists then this function will throw a
 * ReferenceError exception
 */
function of() {
    shorthand('of')
}

/**
 * Creates a function in Array.prototype (and built-in numeric arrays) with
   the name given as a parameter.
   NOTE: If the name already exists (example Array.prototype.at) then this
   function will throw a ReferenceError exception
 * @param {string}  [name] The name of the function (example. of)
 * @param {boolean} [builtInArrays = true] If set to false the function will
                    not hook overflow into the built-in numeric arrays.
 */
function shorthand(name, builtInArrays = true) {
    if (typeof Array.prototype[name] === 'undefined') {
        Array.prototype[name] = prototype
    } else if (Array.prototype[name] !== prototype) {
        throw ReferenceError(`Array.prototype.${name} already exists!`)
    }

    if (builtInArrays) {
        arrays().forEach((array) => {
            if (typeof array.type.prototype[name] === 'undefined') {
                array.type.prototype[name] = prototype
            } else if (array.type.prototype[name] !== prototype) {
                throw ReferenceError(
                    `${array.name}.prototype.${name} already exists!`
                )
            }
        })
    }
}
